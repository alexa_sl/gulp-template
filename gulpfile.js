var os = require('os');
var gulp = require('gulp');
var path = require('path');
var wiredepStream = require('wiredep').stream;
var $ = require('gulp-load-plugins')();

var polyfillsBrowsers = ['last 2 version', 'ie 8', 'ie 9'];
var prefixesBrowsers = [
    'Android 2.3',
    'Android >= 4',
    'Chrome >= 20',
    'Firefox >= 24', // Firefox 24 is the latest ESR
    'Explorer >= 8',
    'iOS >= 6',
    'Opera >= 12',
    'Safari >= 6'
];

var folders = {
    built: 'built',
    js: 'js',
    fonts: 'fonts',
    images: 'img',
    styles: 'styles',
    indexFile: 'index.html'
};

var plumberConfig = {
    errorHandler: function (err) {
        $.util.log($.util.colors.red(err.toString()));
        $.util.beep();
    }
};
var gulpArguments = process.argv.splice(2);
var devServer = {
    host: 'localhost',
    port: 8000
};

var browserApp = (function () {
    //also can be 'firefox'
    var platformBrowserApp;

    switch (os.platform()) {
        case 'linux':
            platformBrowserApp = 'google-chrome';
            break;
        case 'darwin':
            platformBrowserApp = 'open /Applications/Google\\ Chrome.app';
            break;
        case 'win32':
            platformBrowserApp = 'chrome';
            break;
        default:
            $.util.log($.util.colors.red('Unsupported dev platform'));
            process.exit();
            break;
    }

    return platformBrowserApp;
})();

var helpers = {
    addZero: function addZero(n) {
        return (n < 10) ? ("0" + n) : n;
    }
};

gulp.task('default', function () {
    $.runSequence('clean',  ['process-styles', 'process-app-js', 'process-vendors-js', 'process-index'], 'open-browser');
});

gulp.task('clean', function () {
    'use strict';

    return gulp.src([
        path.join(folders.built, '*'),
        path.join(folders.built, 'js'),
        path.join(folders.built, 'css'),
        path.join(folders.built, 'img'),
        path.join(folders.built, 'fonts')
    ], { read: false })
        .pipe($.plumber(plumberConfig))
        .pipe($.rimraf());
});

//gulp.task('clean', function () {
//    return gulp.src(path.join(folders.built, '**', '*'), {read: false})
//        .pipe($.clean());
//});

gulp.task('process-styles', function () {
    'use strict';

    return gulp.src(path.join(folders.styles, 'build.less'))
        .pipe($.plumber(plumberConfig))
        .pipe($.less())
        .pipe($.cssimport())
        .pipe($.size({title: 'Original app styles size: '}))
        .pipe($.autoprefixer({
            browsers: prefixesBrowsers
        }))
        .pipe($.minifyCss({keepBreaks:true}))
        .pipe($.rename({
            basename: 'built'
        }))
        .pipe($.size({title: 'Compressed app styles size: '}))
        .pipe(gulp.dest(path.join(folders.built, 'css')))
        .pipe($.notify({
            message: "Generated file: \n  - <%= file.relative %> \n  - <%= options.date %> \n  - <%= options.author %>",
            templateOptions: {
                date:
                helpers.addZero(new Date().getHours()) + ' : ' +
                helpers.addZero(new Date().getMinutes()) + ' : ' +
                helpers.addZero(new Date().getSeconds()),
                author: 'Alexa'
            }
        }))
});

gulp.task('process-app-js', function () {
    'use strict';

    return gulp.src(folders.indexFile)
        .pipe($.plumber(plumberConfig))
        .pipe($.useref.assets())
        .pipe($.filter(function (file) {
            return /app\.js$/.test(file.path);
        }))
        .pipe($.size({title: 'Original app sources size: '}))
        //.pipe($.uglify())
        .pipe($.size({title: 'Compressed app sources size: '}))
        .pipe(gulp.dest(folders.built));
});

gulp.task('process-index', ['process-app-js', 'process-vendors-js', 'process-styles', 'process-assets'], function () {

    return gulp.src('*.html')
        .pipe($.useref())
        .pipe(gulp.dest(folders.built))
});

gulp.task('process-vendors-js', function () {
    'use strict';

    return gulp.src(folders.indexFile)
        .pipe($.plumber(plumberConfig))
        .pipe(wiredepStream())
        .pipe($.useref.assets())
        .pipe($.filter(function (file) {
            return /vendors\.js$/.test(file.path);
        }))
        .pipe(gulp.dest(folders.built));
});

gulp.task('process-assets-images', function () {
    'use strict';

    return gulp.src(path.join('img', '**', '*.*'))
        .pipe(gulp.dest(path.join(folders.built, 'img')));
});

gulp.task('process-assets-favicon', function () {
    'use strict';

    return gulp.src('favicon.ico')
        .pipe(gulp.dest(folders.built));
});

gulp.task('process-assets-fonts', function () {
    'use strict';

    return gulp.src(path.join('fonts', '**', '*.*'))
        .pipe(gulp.dest(path.join(folders.built, 'fonts')));
});

gulp.task('process-assets-favicon', function () {
    'use strict';

    return gulp.src('*.{png,ico,xml}')
        .pipe(gulp.dest(folders.built));
});

gulp.task('server', function () {
    'use strict';

    return gulp.src(folders.built)
        .pipe($.plumber(plumberConfig))
        .pipe($.webserver({
            host: devServer.host,
            port: devServer.port,
            livereload: true,
            fallback: 'index.html'
        }))
        .pipe($.notify('Dev server was started on: http://' + devServer.host + ':' + devServer.port))
});

gulp.task('open-browser', ['server'], function () {
    'use strict';

    if (gulpArguments.indexOf('--not-open') === -1) {
        gulp.src(folders.built + '/index.html')
            .pipe($.open('', {
                url: 'http://' + devServer.host + ':' + devServer.port,
                app: browserApp
            }));
    }
});

gulp.task('process-assets', ['process-assets-images', 'process-assets-favicon', 'process-assets-fonts', 'process-assets-favicon']);

gulp.task('watchers', ['default'], function () {
    'use strict';

    gulp.watch(path.join(folders.styles, '**', '*.+(less|css)'), ['process-styles']);
    gulp.watch(path.join(folders.js, '**', '*.js'), ['process-index']);
    gulp.watch('*.html', ['process-index']);
    gulp.watch(path.join(folders.images, '**', '*.js'), ['process-assets-images']);
});